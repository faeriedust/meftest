﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MefTest.Interfaces;

namespace MefTest.ExtendedOperations {
  [Export(typeof(IOperation))]
  [ExportMetadata("Symbol", '%')]
  public class ModOperation : IOperation {
    public ModOperation() { }
    public int Operate(int left, int right) {
      return left % right;
    }
  }
}
