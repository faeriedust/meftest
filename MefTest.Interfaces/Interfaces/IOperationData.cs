﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefTest.Interfaces {
  public interface IOperationData {
    char Symbol { get; }
  }
}
