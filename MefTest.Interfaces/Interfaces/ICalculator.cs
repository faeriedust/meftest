﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefTest.Interfaces {
  public interface ICalculator {
    string Calculate(string input);
  }
}
