﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MefTest.Interfaces;

namespace MefTest.Operations {
  [Export(typeof(IOperation))]
  [ExportMetadata("Symbol", '-')]
  public class SubtractOperation : IOperation {
    public SubtractOperation() { }
    public int Operate(int left, int right) {
      return left - right;
    }
  }
}
