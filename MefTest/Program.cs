﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MefTest.Interfaces;

namespace MefTest {
  public class Program {
    #region Initialization
    private Program() { this.Initialize(); }
    private void Initialize() {
      //create catalog with parts
      var catalog = new AggregateCatalog();
      catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
      catalog.Catalogs.Add(new DirectoryCatalog("..\\..\\Extensions"));
      _container = new CompositionContainer(catalog);

      //attempt to fill the imports of this object
      try {
        _container.ComposeParts(this);
      } catch (CompositionException compositionException) {
        Console.WriteLine(compositionException.ToString());
      }
    }
    #endregion
    #region Properties
    private CompositionContainer _container;
    [Import(typeof(ICalculator))]
    public ICalculator Calculator;
    #endregion
    #region Main
    public static void Main(string[] args) {
      var program = new Program(); //Composition is performed in the constructor
      Console.WriteLine("Enter Command:");
      while (true) {
        string input = Console.ReadLine();
        Console.WriteLine(program.Calculator.Calculate(input));
      }
    }
    #endregion
  }
}
