﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MefTest.Interfaces;

namespace MefTest.SimpleCalculator {
  [Export(typeof(ICalculator))]
  public class MySimpleCalculator : ICalculator {
    #region Constructors
    public MySimpleCalculator() { }
    #endregion
    #region Imports
    [ImportMany]
    IEnumerable<Lazy<IOperation, IOperationData>> operations;
    #endregion
    #region ICalculator
    public string Calculate(string input) {
      var operationIndex = _FindFirstOperation(input); //finds the operator
      if (operationIndex < 0) {
        return _ParseErrorMessage;
      }
      int left;
      if (!Int32.TryParse(input.Substring(0, operationIndex), out left)) {
        return _ParseErrorMessage;
      }
      int right;
      if (!Int32.TryParse(input.Substring(operationIndex + 1), out right)) {
        return _ParseErrorMessage;
      }
      char operation = input[operationIndex];
      foreach (Lazy<IOperation, IOperationData> i in operations) {
        if (i.Metadata.Symbol.Equals(operation)) {
          return i.Value.Operate(left, right).ToString();
        }
      }
      return _OperationNotFoundMessage;
    }
    private static int _FindFirstOperation(string input) {
      foreach (int index in Enumerable.Range(0, input.Length)) {
        char c = input[index];
        if (!(Char.IsWhiteSpace(c) || Char.IsDigit(c))) {
          return index;
        }
      }
      return -1;
    }
    #endregion
    #region Messages
    private const string _ParseErrorMessage = "Could not parse command.";
    private const string _OperationNotFoundMessage = "Operation not found!";
    #endregion
  }
}
